package com.example.tugaskalkulator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    TextView Hasil,Operasi;
    MaterialButton buttonC,buttonBrackClose;
    MaterialButton buttonDivide, buttonMultiply, buttonPlus,buttonMinus,
    buttonequals;
    MaterialButton button0,button1,button2,button3,button4,button5,button6,button7,
    button8,button9;
    MaterialButton buttonAC,buttonTitik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Hasil = findViewById(R.id.hasil);
        Operasi = findViewById(R.id.operasi);

        assignId(buttonC,R.id.button_c);
        assignId(buttonC,R.id.button_open_bracket);
        assignId(buttonC,R.id.button_close_bracket);
        assignId(buttonC,R.id.button_divide);
        assignId(buttonC,R.id.button_multiply);
        assignId(buttonC,R.id.button_plus);
        assignId(buttonC,R.id.button_minus);
        assignId(buttonC,R.id.button_equal);
        assignId(buttonC,R.id.button_1);
        assignId(buttonC,R.id.button_2);
        assignId(buttonC,R.id.button_3);
        assignId(buttonC,R.id.button_4);
        assignId(buttonC,R.id.button_5);
        assignId(buttonC,R.id.button_6);
        assignId(buttonC,R.id.button_7);
        assignId(buttonC,R.id.button_8);
        assignId(buttonC,R.id.button_9);
        assignId(buttonC,R.id.button_0);
        assignId(buttonC,R.id.button_AC);
        assignId(buttonC,R.id.button_titik);


    }


    void assignId(MaterialButton btn, int id) {
        btn = findViewById(id);
        btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        MaterialButton button = (MaterialButton) view;
        String buttonText = button.getText().toString();
        String dataToCalculate = Operasi.getText().toString();

        if(buttonText.equals("AC")) {

            Operasi.setText("");
            Hasil.setText("0");
            return;
        }
        if(buttonText.equals("=")) {
            Operasi.setText(Hasil.getText());
            Intent intent = new Intent(this, activity_main2.class );
            intent.putExtra("result",Hasil.getText());
            startActivity(intent);
        return;
        }

        if(buttonText.equals("C")){
            dataToCalculate = dataToCalculate.substring(0,dataToCalculate.length()-1);
        }else{
            dataToCalculate = dataToCalculate + buttonText;
        }

        Operasi.setText(dataToCalculate);

        String finalResult = getResult(dataToCalculate);

        if(!finalResult.equals("Err")) {
            Hasil.setText(finalResult);
        }
    }
    String getResult(String data){
        try{

            Context context = Context.enter();
            context.setOptimizationLevel(-1);
            Scriptable scriptable = context.initStandardObjects();
            String finalResult = context.evaluateString(scriptable,data,"Javascript", 1, null).toString();
            if(finalResult.endsWith(".0")) {
                finalResult = finalResult.replace(".0", "");
            }
            return finalResult;
        } catch (Exception e){
            return"Err";
        }
    }
}