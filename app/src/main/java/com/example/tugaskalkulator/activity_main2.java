package com.example.tugaskalkulator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class activity_main2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        TextView txtHello = findViewById(R.id.txtView);

        Bundle extras = getIntent().getExtras();
        String result = extras.getString("result");
        txtHello.setText(result);

    }
}